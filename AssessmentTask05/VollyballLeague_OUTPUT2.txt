
 Stream 
   Team Name                POINTS    TOTAL_WON  TOTAL_LOSS DRAWN  
   ---------                ------    ---------  ---------- ------
1  Royal kings                253       458        75         0
2  Jaffna fighters            350       470        98         2
3  Galle titans               305       425        92         1
4  Colombo stars              287       380        68         1
5  Kandy floss                297       315        86         2
6  Island rocks               342       360        92         1
7  CSS fighters               283       425        82         3
8  Star worriers              264       350        88         1
9  City Tigers                328       325        72         0
10 Thousand land              333       385        93         3

 Parallel Stream 
   Team Name                POINTS    TOTAL_WON  TOTAL_LOSS DRAWN  
   ---------                ------    ---------  ---------- ------
7  CSS fighters               283       425        82         3
9  City Tigers                328       325        72         0
2  Jaffna fighters            350       470        98         2
6  Island rocks               342       360        92         1
10 Thousand land              333       385        93         3
1  Royal kings                253       458        75         0
8  Star worriers              264       350        88         1
4  Colombo stars              287       380        68         1
3  Galle titans               305       425        92         1
5  Kandy floss                297       315        86         2
