package sports;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;


public class VolleyballLeague01 {
	  public static void main(String[] args) {
	    List<VBClubs> table = Arrays.asList(
	        new VBClubs(1, "Royal kings", 30, 28, 0, 2, 253,1,458,75,26,34,70,111),
	        new VBClubs(2, "Jaffna fighters", 30, 25, 2, 3, 350,2,470,98,40,29,65,123),
	        new VBClubs(3, "Galle titans", 30, 22, 1, 7, 305,0,425,92,40,33,86,133),
	        new VBClubs(4, "Colombo stars", 30, 21, 1, 8, 287,4,380,68,22,25,61,141),
	        new VBClubs(5, "Kandy floss", 30, 18,2,10,297,3,315,86,38,36,83,101),
	        new VBClubs(6, "Island rocks", 30, 16, 1, 13, 342,0,360,92,43,43,75,128),
	        new VBClubs(7, "CSS fighters",30, 12, 3, 15, 283,2,425,82,19,13,91,135),
	        new VBClubs(8, "Star worriers", 30, 10, 1, 19, 264,1,350,88,33,39,67,132),
	        new VBClubs(9, "City Tigers", 30, 9, 0, 21, 328,5,325,72,27,28,83,143),
	        new VBClubs(10, "Thousand land", 30, 7, 3, 20, 333,4,385,93,36,42,85,125));
	 
	    System.out.println("   Team Name                POINTS    TOTAL_WON  TOTAL_LOSS DRAWN  \n");
	     table.forEach(b -> System.out.println(b));
	    
	     try {
				FileWriter writer = new FileWriter("VollyballLeague_OUTPUT1.txt");
				writer.write("   Team Name                POINTS    TOTAL_WON  TOTAL_LOSS DRAWN  \n");
				writer.write("   ---------                ------    ---------  ---------- ------\n");
				table.forEach(x -> {
				try {
					writer.write(x + "\n");
				} catch (IOException e) {
					e.printStackTrace();
				}
			});
				writer.close();
				System.out.println("\nVollyballLeague OUTPUT1.txt file successfully written");
		    } catch (IOException e) {
		    	System.out.println("An error has occurred.");
		    	e.printStackTrace();
		    }

	  }
	  

	}
