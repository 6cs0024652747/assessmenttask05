package sports;

public class VBClubs implements Comparable<VBClubs> {
	  private int rankings;
	  private String clubName;
	  private int matchesPlayed;
	  private int matchesWon;
	  private int matchesDrawn;
	  private int matchesLost;
	  private int pointsScored;
	  private int disqualifiedPerMember;
	  private int totalWon;
	  private int totalLost;
	  private int halfCourt;
	  private int spikesMade;
	  private int blocksMade;
	  private int servesMade;


	  public VBClubs(int rankings, String clubName, int matchesPlayed, int matchesWon, 
			  int matchesDrawn,int matchesLost, int pointsScored, int disqualifiedPerMember, 
			  int totalWon, int totalLost,int halfCourt,int spikesMade, int blocksMade, 
			  int servesMade) {
	    this.rankings = rankings;
	    this.clubName = clubName;
	    this.matchesPlayed = matchesPlayed;
	    this.matchesWon = matchesWon;
	    this.matchesDrawn = matchesDrawn;
	    this.matchesLost = matchesLost;
	    this.pointsScored = pointsScored;
	    this.disqualifiedPerMember = disqualifiedPerMember;
	    this.totalWon = totalWon;
	    this.totalLost = totalLost;
	    this.halfCourt = halfCourt;
	    this.spikesMade = spikesMade;
	    this.blocksMade = blocksMade;
	    this.servesMade = servesMade;
	   
	  }

	  public String toString() {
		  return String.format("%-3d%-20s%10d%10d%10d%10d", rankings, clubName, pointsScored, 
				  totalWon, totalLost,matchesDrawn);
	  }
	  

	  public int getranking() {
	    return rankings;
	  }

	  public void setranking(int ranking) {
	    this.rankings = ranking;
	  }

	  public String getClub() {
	    return clubName;
	  }

	  public void setClub(String club) {
	    this.clubName = club;
	  }

	  public int getPlayed() {
	    return matchesPlayed;
	  }

	  public void setPlayed(int played) {
	    this.matchesPlayed = played;
	  }

	  public int getWon() {
	    return matchesWon;
	  }

	  public void setWon(int won) {
	    this.matchesWon = won;
	  }

	  public int getDrawn() {
	    return matchesDrawn;
	  }

	  public void setDrawn(int drawn) {
	    this.matchesDrawn = drawn;
	  }

	  public int getLost() {
	    return matchesLost;
	  }

	  public void setLost(int lost) {
	    this.matchesLost = lost;
	  }

	  public int gethalfcourt() {
		    return halfCourt;
		  }

	  public void sethalfcourt(int halfcourt) {
		    this.halfCourt = halfcourt;
		  }

	  public int getpointscored() {
		    return pointsScored;
	  }

	  public void setpointscored(int pointscored) {
		    this.pointsScored = pointscored;
	  }

	public int getdisqualifedperMember() {
	    return disqualifiedPerMember;
	  }

	  public void setdisqualifedperMember(int disqualifedperMember) {
	    this.disqualifiedPerMember = disqualifedperMember;
	  }

	  public int totalwon() {
	    return totalWon;
	  }

	  public void settotalwon(int totalwon) {
	    this.totalWon = totalwon;
	  }

	  public int gettotallost() {
	    return totalLost;
	  }

	  public void settotallost(int totallost) {
	    this.totalLost = totallost;
	  }

	  public int getSpikesMade() {
	    return spikesMade;
	  }

	  public void setSpikesMade(int SpikesMade) {
	    this.spikesMade = SpikesMade;
	  }

	  public int getBlocksMade() {
	    return blocksMade;
	  }

	  public void setBlocksMade(int BlocksMade) {
	    this.blocksMade = BlocksMade;
	  }

	  public int getServesMade() {
	    return servesMade;
	  }

	  public void setServesMade(int ServesMade) {
	    this.servesMade = ServesMade;
	  }

	  public int compareTo(VBClubs c) {
	    return ((Integer) disqualifiedPerMember).compareTo(c.disqualifiedPerMember);
	  }
	}
