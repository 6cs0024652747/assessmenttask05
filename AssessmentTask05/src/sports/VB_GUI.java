package sports;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class VB_GUI extends JFrame {
    private JPanel contentPane;

    public VB_GUI() {
        setResizable(false);
        setTitle("Volleyball League");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(600, 600);
        setLocationRelativeTo(null);

        contentPane = new JPanel() {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                Graphics2D g2d = (Graphics2D) g.create();
                GradientPaint gradient = new GradientPaint(0, 0, new Color(135, 129, 150), 0, getHeight(), new Color(70, 130, 180));
                g2d.setPaint(gradient);
                g2d.fillRect(0, 0, getWidth(), getHeight());
                g2d.dispose();
            }
        };

        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(new BorderLayout());

        JLabel title = new JLabel("<html><h1><strong><i>VOLLEYBALL LEAGUE</i></strong></h1><hr></html>");
        title.setHorizontalAlignment(SwingConstants.CENTER);
        title.setFont(new Font("Arial", Font.BOLD, 18));
        title.setForeground(Color.white);
        contentPane.add(title, BorderLayout.NORTH);

        JPanel buttonPanel = new JPanel(new GridLayout(6, 1, 0, 10));
        buttonPanel.setOpaque(false);

        JButton[] volleyballButtons = new JButton[5];
        String[] leagueNames = {"VolleyballLeague01", "VolleyballLeague02", "VolleyballLeague03", "VolleyballLeague04", "VolleyballLeague05"};

        for (int i = 0; i < 5; i++) {
            volleyballButtons[i] = new JButton(leagueNames[i]);
            volleyballButtons[i].addActionListener(new LeagueButtonListener(i + 1));
            volleyballButtons[i].setBackground(new Color(100, 149, 237)); // Cornflower Blue color
            volleyballButtons[i].setForeground(Color.white);
            volleyballButtons[i].setFont(new Font("Arial", Font.BOLD, 14));
            buttonPanel.add(volleyballButtons[i]);
        }

        JButton exit = new JButton("Exit");
        exit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        exit.setBackground(new Color(220, 20, 60)); // Crimson color
        exit.setForeground(Color.white);
        exit.setFont(new Font("Arial", Font.BOLD, 14));
        buttonPanel.add(exit);

        contentPane.add(buttonPanel, BorderLayout.CENTER);
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    VB_GUI frame = new VB_GUI();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    // ActionListener for league buttons
    private class LeagueButtonListener implements ActionListener {
        private int leagueNumber;

        public LeagueButtonListener(int leagueNumber) {
            this.leagueNumber = leagueNumber;
        }

        public void actionPerformed(ActionEvent e) {
            String leagueClassName = "VolleyballLeague0" + leagueNumber;
            try {
                Class<?> leagueClass = Class.forName("sports." + leagueClassName);
                leagueClass.getMethod("main", String[].class).invoke(null, (Object) null);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}
