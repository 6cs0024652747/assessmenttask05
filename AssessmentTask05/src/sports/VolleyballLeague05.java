package sports;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class VolleyballLeague05 {
	public static void main(String[] args) {
	    List<VBClubs> table = Arrays.asList(
	        new VBClubs(1, "Royal kings", 30, 28, 0, 2, 253,1,458,75,26,34,70,111),
	        new VBClubs(2, "Jaffna fighters", 30, 25, 2, 3, 350,2,470,98,40,29,65,123),
	        new VBClubs(3, "Galle titans", 30, 22, 1, 7, 305,0,425,92,40,33,86,133),
	        new VBClubs(4, "Colombo stars", 30, 21, 1, 8, 287,4,380,68,22,25,61,141),
	        new VBClubs(5, "Kandy floss", 30, 18,2,10,297,3,315,86,38,36,83,101),
	        new VBClubs(6, "Island rocks", 30, 16, 1, 13, 342,0,360,92,43,43,75,128),
	        new VBClubs(7, "CSS fighters",30, 12, 3, 15, 283,2,425,82,19,13,91,135),
	        new VBClubs(8, "Star worriers", 30, 10, 1, 19, 264,1,350,88,33,39,67,132),
	        new VBClubs(9, "City Tigers", 30, 9, 0, 21, 328,5,325,72,27,28,83,143),
	        new VBClubs(10, "Thousand land", 30, 7, 3, 20, 333,4,385,93,36,42,85,125));
	    
	    System.out.println("Sorted by Comparator in VBClubs class\n");
	    System.out.println("   Team Name                POINTS    TOTAL_WON  TOTAL_LOSS DRAWN  \n");
	    table.stream().sorted().forEach(System.out::println);

	    System.out.println();
	    System.out.println("Sorted by lambda for Total Won points\n");
	    System.out.println("   Team Name                POINTS    TOTAL_WON  TOTAL_LOSS DRAWN  \n");
	    table.stream()
	         .sorted((v1, v2) -> 
	            ((Integer) v1.totalwon()).compareTo(v2.totalwon()))
	         .forEach(System.out::println);

	  
	  try {
	      FileWriter writer = new FileWriter("VolleyballLeague_OUTPUT5.txt");
	      writer.write("Sorted by Comparator in VBClubs class\n");
	      writer.write("   Team Name                POINTS    TOTAL_WON  TOTAL_LOSS DRAWN  \n");
		  writer.write("   ---------                ------    ---------  ---------- ------\n");
	      table.stream().sorted()
	        .forEach(str -> {
	        	try {
	        		writer.write(str.toString() + "\n");
				} catch (IOException e) {
					e.printStackTrace();
				}
	        });
	      
	      writer.write("\nSorted by lambda for Total Won points\n\n");
	      writer.write("   Team Name                POINTS    TOTAL_WON  TOTAL_LOSS DRAWN  \n");
		  writer.write("   ---------                ------    ---------  ---------- ------\n");
	      table.stream().sorted((c1, c2) -> 
	        ((Integer) c1.totalwon()).compareTo(c2.totalwon()))
	        .forEach(str -> {
	        	try {
	        		writer.write(str.toString() + "\n");
				} catch (IOException e) {
					e.printStackTrace();
				}
	        });
	      writer.close();
	      System.out.println("\nVolleyballLeague OUTPUT5.txt file successfully written.");
	    } catch (IOException e) {
	      System.out.println("An error has occurred.");
	      e.printStackTrace();
	    }
	}


	}

	 

	
